<?php

function a11y_get_enabled_tests() {
  static $tests;
  if ($tests) {
    return $tests;
  }
  $tests = db_select('accessibility_test', 'a')
              ->fields('a', array('quail_name', 'name'))
              ->execute()
              ->fetchAllKeyed();
  return $tests;
}

function a11y_tests_list() {
  $library_path = libraries_get_path('quail');
  drupal_add_js(array('quail_guideline_path' => $library_path . '/src/resources/guidelines/'), 'setting');
  drupal_add_js(drupal_get_path('module', 'a11y') . '/js/a11y.admin.js');
  $options = array();
  $form = array();
  $tests = _a11y_get_quail_tests(true);
  $guidelines = a11y_get_guidelines();
  $guideline_links = array();
  foreach ($guidelines as $name => $label) {
    $guideline_links[] = l($label, '#', array('external' => TRUE, 'attributes' => array('class' => array('guideline'), 'data-guideline' => $name), 'fragment' => ''));
  }

  $form['guidelines'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import entire guideline'),
    '#collapsible' => TRUE,
  );
  
  $form['guidelines']['guideline_links'] = array(
    '#type' => 'markup',
    '#markup' => theme('item_list', array('items' => $guideline_links)),
  );
  $enabled_tests = a11y_get_enabled_tests();
  
  foreach ($tests as $testname => $test) {
    if (!isset($enabled_tests[$testname])) {
      $options[$testname] = t('@name - %severity', array('@name' => t($test->readableName), '%severity' => t($test->severity)));
    }
  }
  
  $form['all_tests'] = array(
    '#type' => 'fieldset',
    '#title' => t('View all tests'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['all_tests']['tests'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available accessibility tests'),
    '#options' => $options,
    '#default_value' => $enabled_tests,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import tests'),
  );
    
  return $form;
}

function a11y_tests_list_submit($form, $form_state) {
  $tests = _a11y_get_quail_tests(true);
  $enabled_tests = a11y_get_enabled_tests();
  $batch = array(
    'operations' => array(),
    'finished' => 'a11y_tests_list_done',
    'title' => t('Importing tests'),
    'init_message' => t('Starting to import tests.'),
    'progress_message' => t('Imported @current out of @total.'),
    'error_message' => t('An error occurred while importing tests.'),
    'file' => drupal_get_path('module', 'a11y') . '/a11y.admin.inc',
  );
  foreach ($form_state['values']['tests'] as $test => $enabled) {
    if ($enabled && !isset($enabled_tests[$test])) {
      $batch['operations'][] = array('_a11y_create_test_from_quail', array($test, $tests[$test]));
    }
  }

  batch_set($batch);
}

function a11y_tests_list_done($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results) . ' imported.';
    $message .= theme('item_list', $results);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
  }
  drupal_set_message($message);
}

function _a11y_create_test_from_quail($quail_name, $test, &$context) {
  $language = language_default();
  $new_test = new stdClass();
  $new_test->language = $language->language;
  $new_test->quail_name = $quail_name;
  $new_test->name = $test->readableName;
  $new_test->severity = ($test->severity) ? $test->severity : 'suggestion';
  $new_test->error_description[$new_test->language][0]['value'] = 
    array('value' => $test->body,
          'format' => 'filtered_html');
  unset($test->readableName);
  unset($test->body);
  $new_test-> data = $test;
  $test = a11y_test_save($new_test);
  $context['message'] = t('Done importing @name', array('@name' => $test->name));
  $context['results'][] = $test->name;
}

function a11y_settings() {
  $form = array();
  
  
  return system_settings_form($form);
}