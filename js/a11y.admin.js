(function($) {
  Drupal.behaviors.a11yAdmin = {
    
    attach : function(context) {
      $('a.guideline').click(function() {
        var guidelinePath = Drupal.settings.basePath + Drupal.settings.quail_guideline_path + $(this).data('guideline') + '.json';
        $.getJSON(guidelinePath, function(data) {
          $.each(data, function(index, test) {
            $(':checkbox[value=' + test + ']').attr('checked', 'checked');
          });
        });
        return false;
      });
    }
  };
})(jQuery);