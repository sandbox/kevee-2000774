<?php

function a11y_testing_admin_form() {
	$form = array();
	
	$form['a11y_testing_paths'] = array(
		'#type' => 'textarea',
		'#title' => t('Enter a list of Drupal paths to test using testswarm'),
		'#default_value' => variable_get('a11y_testing_paths', ''),
	);
	
	return system_settings_form($form);
}