(function($) {
  Drupal.behaviors.a11yContent = {
    
    quailSettings: false,
    
    messages : {},
    
    useBeautyTips : false,
    
    attach: function (context) {
      if(Drupal.settings.a11y_content.use_beautytips && typeof($.bt) !== 'undefined' && $.bt !== null) {
        this.useBeautyTips = true;
      }
      if(Drupal.settings.a11y_content.options.form) {
      	this.checkForm();
      }
      else {
	      if(Drupal.settings.a11y_content.show_toggle) {
	        this.addToggle();
	      }
	      else {
	        if(Drupal.settings.a11y_content.show_default) {
	          this.checkContent();
	        }
	      }
			}
    },
    
    checkForm : function() {
    	var that = this;
    	$.getJSON(Drupal.settings.basePath + 'js/accessibility/tests.json', function(results) {
        that.quailSettings = results;
	    	$('.accessible-cotnent-check-form').each(function() {
	    		var severity = $(this).data('accessible-content-severity').split(',');
	    		$($(this).val()).quail({ guideline : that.quailSettings.guideline,
		        jsonPath : Drupal.settings.basePath + Drupal.settings.a11y_content.quail_path,
		        accessibilityTests : that.quailSettings.tests,
		        testFailed : function(event) {
		          	console.log(event);
		          }
	        });
	    	});
      });
    },
    
    addToggle : function() {
      var that = this;
      var $toggle = $('<div>').attr('id', 'ac-toggle');
      var $link = $('<a>').html(Drupal.settings.a11y_content.toggle.off_message)
                        .attr('href', '#');
      $toggle.append($link);
      $('body').append($toggle);
      $('#ac-toggle a').bind('click', function() {
        if($('body').hasClass('ac-checked')) {
          $(this).html(Drupal.settings.a11y_content.toggle.off_message);
          $('body').removeClass('ac-checked');
          that.removeChecks();
        }
        else {
          $(this).html(Drupal.settings.a11y_content.toggle.on_message);
          that.checkContent();
        }
        return false;
      });
      if(Drupal.settings.a11y_content.show_default) {
        $('#ac-toggle a').click();
      }
    },
    
    checkContent : function() {
      var that = this;
      if($('body').hasClass('ac-checked')) {
        return;
      }
      $('body').addClass('ac-checked');
      if(typeof that.quailSettings === 'object') {
        that.runQuail();
        return;
      }
      $.getJSON(Drupal.settings.basePath + 'js/accessibility/tests.json', function(results) {
        that.quailSettings = results;
        that.runQuail();
      });
    },
    
    removeChecks : function() {
      var that = this;
      $('.ac-result').each(function() {
        $(this).removeClass('ac-result')
               .removeClass('severe')
               .removeClass('moderate')
               .removeClass('suggestion');
        if(that.useBeautyTips) {
          $(this).bt();
        }
        else {
          $(this).unwrap();
        }
      });
    },
    
    runQuail : function() {
      var that = this;
      $('.ac-check-field').quail({ guideline : that.quailSettings.guideline,
        jsonPath : Drupal.settings.basePath + Drupal.settings.a11y_content.quail_path,
        accessibilityTests : that.quailSettings.tests,
        testFailed : function(event) {
          if(!event.element.hasClass('ac-result')) {
            event.element.addClass('ac-result')
                 .addClass(event.severity);
            
            that.attachHint(event);
          }
        }});
    },
    
    attachHint : function(event) {
      var that = this;
      var test = that.quailSettings.tests[event.testName].testId;
      if(!that.messages[test]) {
        $.get(Drupal.settings.basePath + 'accessibility-test/' + test, function(data) {
          that.messages[test] = { message : $(data).find('#content .content').first().html(),
                                  title : $(data).find('h1.title').first().html() };
          that.addHint(event, that.messages[test], test);
        });
      }
      else {
        that.addHint(event, that.messages[test], test);
      }
    },
    
    addHint : function(event, message, id) {      
      if(!$('#ac-hint-' + id).length) {
        var $hint = $('<div>').attr('id', 'ac-hint-' + id)
                              .addClass('ac-hint')
                              .addClass('element-invisible')
                              .append('<h2>' + message.title + '</h2>')
                              .append('<div>' + message.message + '</div>');
        $('body').append($hint);
        $hint.before('<a name="ac-test-' + id +'"></a>');
        if(!this.useBeautyTips) {
          $hint.prepend('<button class="close" title="close">&times;</button>');
          $hint.find('button.close').click(function() {
            $(this).parent('.ac-hint').addClass('element-invisible');
            return false;
          })
        }
      }
      if(this.useBeautyTips) {
        var options = Drupal.settings.beautytips.a11y_content;
        options.closeWhenOthersOpen = true;
        options.contentSelector = "$('#ac-hint-" + id + "').first().html()";
        event.element.bt(options);
      }
      else {
        event.element.wrap('<a href="#ac-test-' + id + '" data-target="#ac-hint-' + id + '" class="ac-link ' + event.severity + '">');
        event.element.parents('.ac-link').click(function() {
          $('.ac-hint').each(function() {
            $(this).addClass('element-invisible');
          })
          $($(this).data('target')).removeClass('element-invisible');
          return false;
        });
      }
    }
  };
})(jQuery);