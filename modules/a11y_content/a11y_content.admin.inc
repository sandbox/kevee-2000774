<?php

function a11y_content_admin_form() {
	$form = array();
  
  $form['a11y_content_auto_check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically check page'),
    '#default_value' => variable_get('a11y_content_auto_check', FALSE),
  );
  
  $form['a11y_content_show_toggle'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show accessibility check toggle'),
    '#default_value' => variable_get('a11y_content_show_toggle', FALSE),
  );
  
  if (module_exists('beautytips')) {
    $form['a11y_content_use_beautytips'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use beautytips to show hints'),
      '#default_value' => variable_get('a11y_content_use_beautytips', TRUE),
    );
  }
  
  $form['toggle'] = array(
    '#type' => 'fieldset',
    '#title' => t('Toggle settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['toggle']['a11y_content_toggle_message_off'] = array(
    '#type' => 'textfield',
    '#title' => t('Message to show when accessibility checking is off'),
    '#default_value' => variable_get('a11y_content_toggle_message_off', t('Check page for accessibility')),
  );
  
  $form['toggle']['a11y_content_toggle_message_on'] = array(
    '#type' => 'textfield',
    '#title' => t('Message to show when accessibility checking is on'),
    '#default_value' => variable_get('a11y_content_toggle_message_on', t('Hide checks for accessibility')),
  );
  
  return system_settings_form($form);
}