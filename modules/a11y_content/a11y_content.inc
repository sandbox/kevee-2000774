<?php

function a11y_content_load_tests_json() {
  $tests = _a11y_content_get_quail_tests();
  $result = array();
  $existing_tests = a11y_get_active_tests();
  foreach ($tests as $test_name => $test) {
    if (isset($existing_tests[$test_name])) {
      $test->a11y_content_test_id = $existing_tests[$test_name];
      $result[$test_name] = $test;
    }
  }
  drupal_json_output(array('guideline' => array_keys($result), 'tests' => $result));
}

function a11y_content_init($options = array()) {
  static $init;
  if ($init) {
    return;
  }
  $init = TRUE;
  if (!user_access('check content for accessibility')) {
    return;
  }
  $library_path = libraries_get_path('quail');
  $settings = array();
  $settings['quail_path'] = $library_path . '/src/resources';
  $settings['show_default'] = variable_get('a11y_content_auto_check', FALSE);
  $settings['show_toggle'] = variable_get('a11y_content_show_toggle', FALSE);
  $settings['toggle']['off_message'] = variable_get('a11y_content_toggle_message_off', t('Check page for accessibility'));
  $settings['toggle']['on_message'] = variable_get('a11y_content_toggle_message_on', t('Hide checks for accessibility'));
  $settings['use_beautytips'] = (module_exists('beautytips') && variable_get('a11y_content_use_beautytips', TRUE));
  $settings['options'] = $options;
  if ($settings['use_beautytips']) {
    $style = variable_get('beautytips_default_style', 'default');
    beautytips_add_beautytips(array('a11y_content' => array('style' => $style)));
  }
  
  $library_path = libraries_get_path('quail');
  drupal_add_js($library_path . '/src/quail.js');
  drupal_add_js(array('a11y_content' => $settings), 'setting');
  drupal_add_js(drupal_get_path('module', 'a11y_content') . '/js/a11y_content.js');
  drupal_add_css(drupal_get_path('module', 'a11y_content') . '/css/a11y_content.css');
}

function _a11y_content_field_pre_render($element) {
  $element['#prefix'] = (isset($element['#prefix'])) ? $element['#prefix'] : '';
  $element['#suffix'] = (isset($element['#suffix'])) ? $element['#suffix'] : '';
  $element['#prefix'] .= '<span class="ac-check-field">';
  $element['#suffix'] .= '</span>';
  return $element;
}