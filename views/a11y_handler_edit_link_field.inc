<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */


class a11y_handler_edit_link_field extends a11y_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['test_id'] = 'test_id';
  }


  function render($values) {
    $type = $values->{$this->aliases['test_id']};
    
    //Creating a dummy a11y to check access against
    $dummy_a11y = (object) array('type' => $type);
    if (!a11y_test_access('edit', $dummy_a11y)) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $a11y_id = $values->{$this->aliases['test_id']};
    
    return l($text, 'accessibility-test/' . $a11y_id . '/edit');
  }
}
