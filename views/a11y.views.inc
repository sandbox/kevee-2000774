<?php

/**
 * @file
 * Providing extra functionality for views.
 */
function a11y_views_data() {
  // Define the base group of this table.
  $data['accessibility_test']['table']['group']  = t('Accessibility test');

  // Advertise this table as a possible base table
  $data['accessibility_test']['table']['base'] = array(
    'field' => 'test_id',
    'title' => t('Accessibility test'),
    'weight' => -10,
  );
  $data['accessibility_test']['table']['entity type'] = 'accessibility_test';
  $data['accessibility_test']['test_id'] = array(
    'title' => t('Test ID'),
    'help' => t('The test ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['accessibility_test']['name'] = array(
    'title' => t('Name'),
    'help' => t('The test name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'relation_get_types_options',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['accessibility_test']['severity'] = array(
    'title' => t('Severity'),
    'help' => t('The severity level of the test.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'relation_get_types_options',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['accessibility_test']['quail_name'] = array(
    'title' => t('QUAIL Name'),
    'help' => t('The name of the QUAIL test to be called.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'relation_get_types_options',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['accessibility_test']['status'] = array(
    'field' => array(
      'title' => t('Test status'),
      'help' => t('Show the status of the test.'),
      'handler' => 'a11y_handler_status',
    ),
  );

  $data['accessibility_test']['link_a11y'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the test.'),
      'handler' => 'a11y_handler_link_field',
    ),
  );
  
  $data['accessibility_test']['edit_a11y'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the a11y.'),
      'handler' => 'a11y_handler_edit_link_field',
    ),
  );
  
  $data['accessibility_test']['delete_a11y'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the a11y.'),
      'handler' => 'a11y_handler_delete_link_field',
    ),
  );
    
  return $data;
}

/**
 * Implements hook_views_default_views().
 */
function a11y_views_default_views() {
  $view = new view();
  $view->name = 'ac_accessibility_tests';
  $view->description = 'Provides an administrative interface for managing existing tests.';
  $view->tag = 'default';
  $view->base_table = 'accessibility_test';
  $view->human_name = 'Existing accessibility tests';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Existing accessibility tests';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Accessibility test: Test ID */
  $handler->display->display_options['fields']['test_id']['id'] = 'test_id';
  $handler->display->display_options['fields']['test_id']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['test_id']['field'] = 'test_id';
  /* Field: Accessibility test: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Accessibility test: QUAIL Name */
  $handler->display->display_options['fields']['quail_name']['id'] = 'quail_name';
  $handler->display->display_options['fields']['quail_name']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['quail_name']['field'] = 'quail_name';
  /* Field: Accessibility test: Severity */
  $handler->display->display_options['fields']['severity']['id'] = 'severity';
  $handler->display->display_options['fields']['severity']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['severity']['field'] = 'severity';
  /* Field: Accessibility test: Test status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['label'] = 'Status';
  /* Field: Accessibility test: Edit Link */
  $handler->display->display_options['fields']['edit_a11y']['id'] = 'edit_a11y';
  $handler->display->display_options['fields']['edit_a11y']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['edit_a11y']['field'] = 'edit_a11y';
  $handler->display->display_options['fields']['edit_a11y']['label'] = 'Edit';
  /* Field: Accessibility test: Link */
  $handler->display->display_options['fields']['link_a11y']['id'] = 'link_a11y';
  $handler->display->display_options['fields']['link_a11y']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['link_a11y']['field'] = 'link_a11y';
  $handler->display->display_options['fields']['link_a11y']['label'] = 'View';
  /* Field: Accessibility test: Delete Link */
  $handler->display->display_options['fields']['delete_a11y']['id'] = 'delete_a11y';
  $handler->display->display_options['fields']['delete_a11y']['table'] = 'accessibility_test';
  $handler->display->display_options['fields']['delete_a11y']['field'] = 'delete_a11y';
  $handler->display->display_options['fields']['delete_a11y']['label'] = 'Delete';
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'admin/config/accessibility/tests/existing';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Existing tests';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;

  $views[] = $view;
  return $views;
}