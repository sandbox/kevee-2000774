<?php

function a11y_tests_json($return = false) {
	$existing_tests = a11y_get_active_tests();
	$result = array();
  foreach ($existing_tests as $test_name => $test) {
    if (isset($existing_tests[$test_name])) {
      $test->accessible_content_test_id = $existing_tests[$test_name];
      $result[$test->quail_name] = $test->data;
      $result[$test->quail_name]->readableName = $test->name;
      $result[$test->quail_name]->testId = $test->test_id;
    }
  }
  if($return) {
  	return $result;
  }
  drupal_json_output(array('guideline' => array_keys($result), 'tests' => $result));
}